#!/bin/sh

PROJECT_ID=PRJNA231909
PROJ_DIR=$HOME/microbiome/$PROJECT_ID
FASTQ_DIR=$PROJ_DIR/fastq
METAPHLAN_DIR=$PROJ_DIR/metaphlan2

threads=4
SCRIPT=$HOME/microbiome/script/metaphlan2/trim_galore_job.sh

OUT=$METAPHLAN_DIR/out1_trim_galore
if [ ! -d ${OUT} ]; then
 mkdir -p ${OUT}
fi

list=$(ls $FASTQ_DIR/*.fastq.gz | xargs -n1 basename | awk 'BEGIN{FS="_"}{ print $1 }' | sort | uniq)

echo $list
COUNT=0
for file in $list; do
 COUNT=$(( $COUNT + 1 ))
 echo "Job submitted for ${COUNT} --> ${file} ..."
 R1=$FASTQ_DIR/${file}_pe_1.fastq.gz
 R2=$FASTQ_DIR/${file}_pe_2.fastq.gz
 if [ -e $R1 ] && [ -e $R2 ]; then
   echo $R1 " --> " $R2
   qsub -v SID=$file,R1=$R1,R2=$R2,OUT=$OUT,t=$threads, \
    -N trim${file} \
    -q b.q \
    -pe smp $threads \
    -j y -m abe -M <email_id> \
    -cwd $SCRIPT

    sleep 2

 else
   echo "one or more is missing"
 fi
done

echo "Finish - `date`"
