#!/bin/sh

PROJECT_ID=PRJNA231909
PROJ_DIR=$HOME/microbiome/$PROJECT_ID
METAPHLAN_DIR=$PROJ_DIR/metaphlan2
FASTQ_DIR=$METAPHLAN_DIR/out2_vsearch
# FASTQ_DIR=$PROJ_DIR/fastq
rRNA_DB=$HOME/sortmerna-2.1b/rRNA_databases
rRNA_INDEX=$HOME/sortmerna-2.1b/index
THREADS=1
SCRIPT=$HOME/microbiome/script/metaphlan2/sortmerna_job.sh

OUT=$METAPHLAN_DIR/out3_sortmerna
if [ ! -d ${OUT} ]; then
 mkdir -p ${OUT}
fi

list=$(ls $FASTQ_DIR/*.sorted.fasta | xargs -n1 basename | awk 'BEGIN{FS="."}{ print $1 }' | sort | uniq)

echo $list
# https://www.cell.com/cms/attachment/2054936429/2060898677/mmc1.pdf?code=cell-site
COUNT=0
for file in $list; do
 COUNT=$(( $COUNT + 1 ))
 echo "Job submitted for ${COUNT} --> ${file} ..."
 #R1=$FASTQ_DIR/${file}_pe_1.fastq.gz
 #R2=$FASTQ_DIR/${file}_pe_2.fastq.gz
 R=$FASTQ_DIR/${file}.sorted.fasta
 # if [ -e $R1 ] && [ -e $R2 ]; then
 if [ -e $R ]; then
   echo $R
   qsub -v SID=$file,rRNA_DB=$rRNA_DB,rRNA_INDEX=$rRNA_INDEX,R=$R,OUT=$OUT,THREADS=$THREADS, \
    -N rm.${file} \
    -q b.q \
    -pe smp $THREADS \
    -j y -m abe -M <email_id> \
    -cwd $SCRIPT

    sleep 2

 else
   echo "one or more is missing"
 fi
done

echo "Finish - `date`"
