#!/bin/sh

# HUMAnN2 (the HMP Unified Metabolic Analysis Network 2

PROJECT_ID=PRJNA231909
PROJ_DIR=$HOME/microbiome/$PROJECT_ID
METAPHLAN_DIR=$PROJ_DIR/metaphlan2

OUT=$METAPHLAN_DIR/out4_humann2

if [ ! -d ${OUT} ]; then
 mkdir -p ${OUT}
fi

FASTA_DIR=$METAPHLAN_DIR/out3_sortmerna
threads=10
SCRIPT=$HOME/microbiome/script/metaphlan2/humann2_job.sh

# HUMAnN2 configuration file updated: database_folders : protein = /home/adinasarapu/uniref

list=$(ls $FASTA_DIR/*_non_rRNA.fasta | xargs -n1 basename | awk 'BEGIN{FS="_non"}{ print $1 }' | sort | uniq)

# G4525[0-1]_non_rRNA.fasta
# G4525[0-1]*

echo $list
arr=${list[@]}

# convert a bash array into a comma delimited string
all_job_ids=${arr// /,}

COUNT=0
for file in $list; do
 COUNT=$(( $COUNT + 1 ))
 echo "Job submitted for ${COUNT} --> ${file} ..."

 R1=$FASTA_DIR/${file}_non_rRNA.fasta
 if [ -e $R1 ]; then
   echo $R1
   qsub -v SID=$file,R1=$R1,OUT=$OUT,t=$threads, \
    -N ${file} \
    -q b.q \
    -pe smp $threads \
    -j y -m abe -M <email_id> \
    -cwd $SCRIPT

    sleep 1

 else
   echo "raw fastq file is missing"
 fi
done

SCRIPT_JOIN=$HOME/microbiome/script/metaphlan2/humann2_join_job.sh
HUMANN_OUT=$OUT/*_humann2

if [ -e $SCRIPT_JOIN ]; then
 echo "Job submitted ..."
 echo $all_job_ids
 qsub -hold_jid $all_job_ids -v R=$HUMANN_OUT,OUT=$OUT,t=2, \
  -N join.humann2 \
  -q b.q \
  -pe smp 1 \
  -j y -m abe -M ashok.reddy.dinasarapu@emory.edu \
  -cwd $SCRIPT_JOIN
 else
  echo "humann2 results missing"
fi

echo "Finish - `date`"
