#!/bin/sh

# A comprehensive metagenomic analysis from whole-genome shotgun sequencing data for microbiome studies
# https://salsa.debian.org/med-team/metaphlan2

# Download T1D data from
# https://pubs.broadinstitute.org/diabimmune/t1d-cohort/resources/metagenomic-sequence-data

echo "Start - `date`" 

echo ---------------------------------
echo -e `cat $PE_HOSTFILE`
echo ---------------------------------
echo SGE: qsub is running on $HOSTNAME
echo SGE: job identifier is $JOB_ID
echo SGE: job name is $JOB_NAME
echo SGE: parallel environment $PE
echo ---------------------------------

# sortmerna (installed in home)

TMP_NAME=`/usr/bin/mktemp -u XXXXXXXX`
TMP_DIR="/scratch/${JOB_ID}_${TMP_NAME}"
if [ -e $TMP_DIR ]; then
   echo "Error. Scratch dir already exists on `hostname`"
   exit
else
    mkdir $TMP_DIR
fi

chmod a+trwx $TMP_DIR
mkdir -p $TMP_DIR/{fastq,vsearch}
echo $TMP_DIR

rsync -av $R1 $TMP_DIR/fastq/
rsync -av $R2 $TMP_DIR/fastq/

# Paired-end reads merging

# --fastq_mergepairs FILENAME merge paired-end reads into one sequence 
# --fastaout_notmerged_fwd FN FASTA filename for non-merged forward sequences
# --fastaout_notmerged_rev FN FASTA filename for non-merged reverse sequences
# --fastq_eeout               include expected errors in FASTQ output
# --fastq_minovlen            minimum length of overlap between reads (10)
# --fastq_maxdiffs INT        maximum number of different bases in overlap (10)

# Derepliaction is not a filtering process! This step is used to compute the abundance of reads. 
# --derep_fulllength FILENAME dereplicate sequences in the given FASTA file

echo "Processing sample: $SID" 

VSEARCH=$(which vsearch)

$VSEARCH --threads $THREADS \
 --fastq_mergepairs $TMP_DIR/fastq/${SID}_pe_1_val_1.fq.gz \
 --reverse $TMP_DIR/fastq/${SID}_pe_2_val_2.fq.gz \
 --fastq_minovlen 10 \
 --fastq_maxdiffs 10 \
 --fastaout $TMP_DIR/vsearch/${SID}.merged.fasta \
 --fastqout_notmerged_fwd $TMP_DIR/vsearch/${SID}_pe_1_notmerged.fastq \
 --fastqout_notmerged_rev $TMP_DIR/vsearch/${SID}_pe_2_notmerged.fastq

/bin/rm -rf $TMP_DIR/fastq

# Dereplication <<<USING VSEARCH>>>

# https://github.com/torognes/vsearch/wiki/VSEARCH-pipeline
# --fasta_width 0           width of FASTA seq lines, 0 for no wrap (80)
# --relabel STRING            relabel sequences with this prefix string

# --sizeout                   write abundance annotation to output
# --minuniquesize 2           minimum abundance for output from dereplicatio (also removes singletons)

# --uc FILENAME               filename for UCLUST-like output
# --strand plus|both          cluster using plus or both strands (plus)
 
$VSEARCH --threads $THREADS \
 --derep_fulllength $TMP_DIR/vsearch/${SID}.merged.fasta \
 --output $TMP_DIR/vsearch/${SID}.derep.fasta \
 --sizeout \
 --uc $TMP_DIR/vsearch/${SID}.derep.uc --strand plus \
 --relabel ${SID}. \
 --fasta_width 0

# Abundance sort and discard singletons <<<USING VSEARCH>>>
 
$VSEARCH --threads $THREADS \
 --sortbysize $TMP_DIR/vsearch/${SID}.derep.fasta \
 --output $TMP_DIR/vsearch/${SID}.sorted.fasta \
 --minsize 2

# Chimera detection and removal (for reference-based chimera search use --uchime_ref)

# Greengenes-based alignments http://www.mothur.org/wiki/Greengenes-formatted_databases
# SILVA reference data http://www.mothur.org/wiki/Silva_reference_files

# The total number of chimeras + non-chimeras + borderline should add up to the total number of input sequences.

$VSEARCH --threads $THREADS \
 --uchime_denovo $TMP_DIR/vsearch/${SID}.sorted.fasta \
 --chimeras $TMP_DIR/vsearch/${SID}.chimera.fasta \
 --nonchimeras $TMP_DIR/vsearch/${SID}.non.chimera.fasta \
 --borderline $TMP_DIR/vsearch/${SID}.borderline.chimera.fasta

rsync -av $TMP_DIR/vsearch/* $OUT/

/bin/rm -rf $TMP_DIR

echo "Finish - `date`"
