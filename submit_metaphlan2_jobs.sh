#!/bin/sh

# A comprehensive metagenomic analysis from whole-genome shotgun sequencing data for microbiome studies
# https://salsa.debian.org/med-team/metaphlan2

# Download T1D data from
# https://pubs.broadinstitute.org/diabimmune/t1d-cohort/resources/metagenomic-sequence-data

PROJECT_ID=PRJNA231909
PROJ_DIR=$HOME/microbiome/$PROJECT_ID
METAPHLAN_DIR=$PROJ_DIR/metaphlan2
FASTQ_DIR=$PROJ_DIR/fastq
threads=4
SCRIPT=$HOME/microbiome/script/metaphlan2/metaphlan2_job.sh

OUT=$METAPHLAN_DIR/metaphlan2_out
if [ ! -d ${OUT} ]; then
 mkdir -p ${OUT}
fi

list=$(ls $FASTQ_DIR/*.fastq.gz | xargs -n1 basename | awk 'BEGIN{FS="_"}{ print $1 }' | sort | uniq)

# G4525[0-1]*
# ls microbiome/PRJNA231909/fastq/G4525[0-1]* | xargs -n1 basename | awk 'BEGIN{FS="_"}{ print $1 }' | sort | uniq

echo $list
# https://www.cell.com/cms/attachment/2054936429/2060898677/mmc1.pdf?code=cell-site
COUNT=0
for file in $list; do
 COUNT=$(( $COUNT + 1 ))
 echo "Job submitted for ${COUNT} --> ${file} ..."
 R1=$FASTQ_DIR/${file}_pe_1.fastq.gz
 R2=$FASTQ_DIR/${file}_pe_2.fastq.gz
 if [ -e $R1 ] && [ -e $R2 ]; then
   echo $R1 " --> " $R2
   qsub -v SID=$file,R1=$R1,R2=$R2,OUT=$OUT,t=$threads, \
    -N ${file}.MetaPhlAn2 \
    -q b.q \
    -pe smp $threads \
    -j y -m abe -M <email_id> \
    -cwd $SCRIPT

    sleep 5

 else
   echo "one or more is missing"
 fi
done

sleep 5

# To make a tab-delimited (species relative abundances) single table
# run the following command
 
# merge_metaphlan_tables.py metaphlan2_out/*_profile.txt > PRJNA231909.tsv

# For HUMAnN2: The HMP Unified Metabolic Analysis Network 2
# https://bitbucket.org/biobakery/biobakery/wiki/Home 
# conda install humann2

# to count number of jobs running at cluster
# qstat -xml -f -u \* | fgrep JB_name | wc -l
# qstat -xml -f | fgrep JB_name | wc -l

echo "Finish - `date`"
