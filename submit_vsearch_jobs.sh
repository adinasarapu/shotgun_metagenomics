#!/bin/sh

PROJECT_ID=PRJNA231909
PROJ_DIR=$HOME/microbiome/$PROJECT_ID
METAPHLAN_DIR=$PROJ_DIR/metaphlan2
FASTQ_DIR=$METAPHLAN_DIR/out1_trim_galore

THREADS=2
SCRIPT=$HOME/microbiome/script/metaphlan2/vsearch_job.sh

OUT=$METAPHLAN_DIR/out2_vsearch
if [ ! -d ${OUT} ]; then
 mkdir -p ${OUT}
fi

#echo Obtaining Gold reference database for chimera detection
#
#if [ ! -e gold.fasta ]; then
#
#    if [ ! -e Silva.gold.bacteria.zip ]; then
#        wget https://www.mothur.org/w/images/f/f1/Silva.gold.bacteria.zip
#    fi
#
#    echo Decompressing and reformatting...
#    unzip -p Silva.gold.bacteria.zip silva.gold.align | \
#        sed -e "s/[.-]//g" > gold.fasta
#
#fi

# REF=gold.fasta

list=$(ls $FASTQ_DIR/*.fq.gz | xargs -n1 basename | awk 'BEGIN{FS="_"}{ print $1 }' | sort | uniq)

# G4525[0-1]*
# ls /*.fq.gz | xargs -n1 basename | awk 'BEGIN{FS="_"}{ print $1 }' | sort | uniq

echo $list
COUNT=0
for file in $list; do
 COUNT=$(( $COUNT + 1 ))
 echo "Job submitted for ${COUNT} --> ${file} ..."
 R1=$FASTQ_DIR/${file}_pe_1_val_1.fq.gz
 R2=$FASTQ_DIR/${file}_pe_2_val_2.fq.gz
 if [ -e $R1 ] && [ -e $R2 ]; then
   echo $R1 " --> " $R2
   qsub -v SID=$file,R1=$R1,R2=$R2,OUT=$OUT,THREADS=$THREADS, \
    -N drep${file} \
    -q b.q \
    -pe smp $THREADS \
    -j y -m abe -M <email_id> \
    -cwd $SCRIPT

    sleep 2

 else
   echo "one or more is missing"
 fi
done

echo "Finish - `date`"
