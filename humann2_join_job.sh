#!/bin/sh

# A comprehensive metagenomic analysis from whole-genome shotgun sequencing data for microbiome studies
# https://salsa.debian.org/med-team/metaphlan2

# Download T1D data from
# https://pubs.broadinstitute.org/diabimmune/t1d-cohort/resources/metagenomic-sequence-data

echo "Start - `date`" 

echo ---------------------------------
echo -e `cat $PE_HOSTFILE`
echo ---------------------------------
echo SGE: qsub is running on $HOSTNAME
echo SGE: job identifier is $JOB_ID
echo SGE: job name is $JOB_NAME
echo SGE: parallel environment $PE
echo ---------------------------------

# sortmerna (installed in home)

TMP_NAME=`/usr/bin/mktemp -u XXXXXXXX`
TMP_DIR="/scratch/${JOB_ID}_${TMP_NAME}"
if [ -e $TMP_DIR ]; then
   echo "Error. Scratch dir already exists on `hostname`"
   exit
else
    mkdir $TMP_DIR
fi

# humann2 installed as a part of bioconda
source activate ddocent_env

# Download the ChocoPhlAn database

# humann2_databases --download chocophlan full $HOME
# HUMAnN2 configuration file updated: database_folders : nucleotide = /home/adinasarapu/chocophlan

# Download a translated search database
# While there is only one ChocoPhlAn database, users have a choice of 
# translated search databases which offer trade-offs between resolution, 
# coverage, and performance. For  EC-filtered UniRef90 database (0.8GB)

# humann2_databases --download uniref uniref90_ec_filtered_diamond $HOME
# HUMAnN2 configuration file updated: database_folders : protein = /home/adinasarapu/uniref

# exclude a subdirectory under $R/ that matches the pattern “*_temp”
rsync -av --exclude '*_temp' $R $TMP_DIR
ls $TMP_DIR/*

# 3. Join the output files (gene families, coverage, and abundance) from the HUMAnN2 runs from all samples into three files
humann2_join_tables \
 --input $TMP_DIR/ \
 --output $TMP_DIR/humann2_genefamilies.tsv \
 --file_name genefamilies_relab \
 --search-subdirectories

humann2_join_tables \
 --input $TMP_DIR/ \
 --output $TMP_DIR/humann2_pathcoverage.tsv \
 --file_name pathcoverage \
 --search-subdirectories

humann2_join_tables \
 --input $TMP_DIR/ \
 --output $TMP_DIR/humann2_pathabundance.tsv \
 --file_name pathabundance_relab \
 --search-subdirectories

# HUMAnN2 utility scripts
# humann2_infer_taxonomy requires a data file to link UniRef families to LCAs and infer taxonomic relationships
# humann2_databases --download utility_mapping full $DIR
# utility_mapping = /home/adinasarapu/utility_mapping

#  humann2_config  --print
 
humann2_infer_taxonomy \
 --input $TMP_DIR/humann2_genefamilies.tsv \
 --level Genus \
 --resolution uniref90 \
 --mode totals \
 --output $TMP_DIR/humann2_genefamilies_taxonomy.tsv

# --input HUMAnN2 table with optional metadata
#humann2_associate \
# --input $TMP_DIR/humann2_pathabundance.tsv \
# --last-metadatum STSite \
# --focal-metadatum STSite \
# --focal-type categorical \
# --fdr 0.2 \
# --output $TMP_DIR/stats.txt

#humann2_barplot \
# --input hmp_pathabund.pcl \
# --focal-feature METSYN-PWY \
# --focal-metadatum STSite \
# --last-metadatum STSite \
# --sort sum \
# --output plot1.png

# https://bitbucket.org/biobakery/humann2/wiki/Home#markdown-header-5-download-the-databases

# module unload bowtie2/2.2.6
source deactivate ddocent_env

rsync -av $TMP_DIR/humann2_genefamilies.tsv $OUT
rsync -av $TMP_DIR/humann2_pathcoverage.tsv $OUT
rsync -av $TMP_DIR/humann2_pathabundance.tsv $OUT

/bin/rm -rf $TMP_DIR

echo "Finish - `date`"
