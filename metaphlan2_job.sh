#!/bin/sh

# A comprehensive metagenomic analysis from whole-genome shotgun sequencing data for microbiome studies
# https://salsa.debian.org/med-team/metaphlan2

# Download T1D data from
# https://pubs.broadinstitute.org/diabimmune/t1d-cohort/resources/metagenomic-sequence-data

echo "Start - `date`" 

echo ---------------------------------
echo -e `cat $PE_HOSTFILE`
echo ---------------------------------
echo SGE: qsub is running on $HOSTNAME
echo SGE: job identifier is $JOB_ID
echo SGE: job name is $JOB_NAME
echo SGE: parallel environment $PE
echo ---------------------------------

# bioconda (metaphlan2 installed)
# Check if the MetaPhlAn2 DB is installed and install it if not. All other parameters are ignored
# metaphlan2.py --install

source activate ddocent_env
module load bowtie2/2.2.6

TMP_NAME=`/usr/bin/mktemp -u XXXXXXXX`
TMP_DIR="/scratch/${JOB_ID}_${TMP_NAME}"
if [ -e $TMP_DIR ]; then
   echo "Error. Scratch dir already exists on `hostname`"
   exit
else
    mkdir $TMP_DIR
fi

chmod a+trwx $TMP_DIR
mkdir -p $TMP_DIR/fastq
echo $TMP_DIR

rsync -av $R1 $TMP_DIR/fastq/
rsync -av $R2 $TMP_DIR/fastq/

# .bowtie2out.txt Contains a mapping of reads to MetaPhlAn markers
# _profile.txt contains taxonomic abundances as percentages
# zcat $R1 $R2 | gzip -c > $TMP_DIR/${SID}_pe.fastq.gz
 
zcat $TMP_DIR/fastq/*_pe_1.fastq.gz $TMP_DIR/fastq/*_pe_2.fastq.gz | \
 metaphlan2.py --input_type fastq --no_map \
 --nproc $t > $TMP_DIR/${SID}_profile.txt

module unload bowtie2/2.2.6
source deactivate ddocent_env

/bin/rm -rf $TMP_DIR/fastq

rsync -av $TMP_DIR/* $OUT

/bin/rm -rf $TMP_DIR

echo "Finish - `date`"
