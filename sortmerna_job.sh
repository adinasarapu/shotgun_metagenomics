#!/bin/sh

# A comprehensive metagenomic analysis from whole-genome shotgun sequencing data for microbiome studies
# https://salsa.debian.org/med-team/metaphlan2

# Download T1D data from
# https://pubs.broadinstitute.org/diabimmune/t1d-cohort/resources/metagenomic-sequence-data

echo "Start - `date`" 

echo ---------------------------------
echo -e `cat $PE_HOSTFILE`
echo ---------------------------------
echo SGE: qsub is running on $HOSTNAME
echo SGE: job identifier is $JOB_ID
echo SGE: job name is $JOB_NAME
echo SGE: parallel environment $PE
echo ---------------------------------

# sortmerna (installed in home)

TMP_NAME=`/usr/bin/mktemp -u XXXXXXXX`
TMP_DIR="/scratch/${JOB_ID}_${TMP_NAME}"
if [ -e $TMP_DIR ]; then
   echo "Error. Scratch dir already exists on `hostname`"
   exit
else
    mkdir $TMP_DIR
fi

chmod a+trwx $TMP_DIR
# mkdir -p $TMP_DIR/{fastq,merged}
mkdir -p $TMP_DIR/fasta
echo $TMP_DIR

# rsync -av $R1 $TMP_DIR/fastq/
# rsync -av $R2 $TMP_DIR/fastq/
rsync -av $R $TMP_DIR/fasta/

rsync -av $rRNA_DB $TMP_DIR
rsync -av $rRNA_INDEX $TMP_DIR

# zcat $TMP_DIR/fastq/${SID}_pe_1.fastq.gz > $TMP_DIR/fastq/${SID}_pe_1.fastq
# zcat $TMP_DIR/fastq/${SID}_pe_2.fastq.gz > $TMP_DIR/fastq/${SID}_pe_2.fastq

# ~/sortmerna-2.1b/scripts/merge-paired-reads.sh \
#  $TMP_DIR/fastq/${SID}_pe_1.fastq \
#  $TMP_DIR/fastq/${SID}_pe_2.fastq \
#  $TMP_DIR/merged/${SID}.fastq

# head -n 30 $TMP_DIR/merged/${SID}.fastq

# /bin/rm -rf $TMP_DIR/fastq

# ls $TMP_DIR/merged/*
ls $TMP_DIR/rRNA_databases/*
ls $TMP_DIR/index/*

# --paired_in

sortmerna --ref \
$TMP_DIR/rRNA_databases/rfam-5.8s-database-id98.fasta,$TMP_DIR/index/rfam-5.8s-db:\
$TMP_DIR/rRNA_databases/silva-arc-16s-id95.fasta,$TMP_DIR/index/silva-arc-16s-db:\
$TMP_DIR/rRNA_databases/silva-bac-16s-id90.fasta,$TMP_DIR/index/silva-bac-16s-db:\
$TMP_DIR/rRNA_databases/silva-euk-18s-id95.fasta,$TMP_DIR/index/silva-euk-18s-db:\
$TMP_DIR/rRNA_databases/rfam-5s-database-id98.fasta,$TMP_DIR/index/rfam-5s-db:\
$TMP_DIR/rRNA_databases/silva-arc-23s-id98.fasta,$TMP_DIR/index/silva-arc-23s-db:\
$TMP_DIR/rRNA_databases/silva-bac-23s-id98.fasta,$TMP_DIR/index/silva-bac-23s-db:\
$TMP_DIR/rRNA_databases/silva-euk-28s-id98.fasta,$TMP_DIR/index/silva-euk-28s-db \
 --reads $TMP_DIR/fasta/${SID}.sorted.fasta \
 --fastx \
 --aligned $TMP_DIR/${SID}_rRNA \
 --other $TMP_DIR/${SID}_non_rRNA \
 -a $THREADS \
 --log \
 -v

/bin/rm -rf $TMP_DIR/rRNA_databases
/bin/rm -rf $TMP_DIR/index
/bin/rm -rf $TMP_DIR/fasta

rsync -av $TMP_DIR/* $OUT

/bin/rm -rf $TMP_DIR

echo "Finish - `date`"
