#!/bin/sh

# A comprehensive metagenomic analysis from whole-genome shotgun sequencing data for microbiome studies
# https://salsa.debian.org/med-team/metaphlan2

# Download T1D data from
# https://pubs.broadinstitute.org/diabimmune/t1d-cohort/resources/metagenomic-sequence-data

echo "Start - `date`" 

echo ---------------------------------
echo -e `cat $PE_HOSTFILE`
echo ---------------------------------
echo SGE: qsub is running on $HOSTNAME
echo SGE: job identifier is $JOB_ID
echo SGE: job name is $JOB_NAME
echo SGE: parallel environment $PE
echo ---------------------------------

# sortmerna (installed in home)

TMP_NAME=`/usr/bin/mktemp -u XXXXXXXX`
TMP_DIR="/scratch/${JOB_ID}_${TMP_NAME}"
if [ -e $TMP_DIR ]; then
   echo "Error. Scratch dir already exists on `hostname`"
   exit
else
    mkdir $TMP_DIR
fi

chmod a+trwx $TMP_DIR
mkdir -p $TMP_DIR/{fastq,trimmed}
echo $TMP_DIR

rsync -av $R1 $TMP_DIR/fastq/
rsync -av $R2 $TMP_DIR/fastq/

# cutadapt installed through bioconda

module load FastQC/0.11.4 

trim_galore $TMP_DIR/fastq/${SID}_pe_1.fastq.gz $TMP_DIR/fastq/${SID}_pe_2.fastq.gz \
 --paired \
 -q 20 \
 --phred33 \
 --fastqc \
 --length 20 \
 --stringency 5 \
 --gzip \
 -o $TMP_DIR/trimmed/
 
module unload FastQC/0.11.4

/bin/rm -rf $TMP_DIR/fastq

rsync -av $TMP_DIR/trimmed/* $OUT/

/bin/rm -rf $TMP_DIR

echo "Finish - `date`"
