# shotgun_metagenomics

## 1. NGS read processing for quality control and adaptor removal using TrimGalore.

Run QC (filtering)

`submit_trim_galore_jobs.sh`
`trim_galore_job.sh`

## 2. Merging and dereplication of reads using vsearch

Run vsearch (merging)

`submit_vsearch_jobs.sh`
`vsearch_job.sh`

## 3a. Taxonomic profiling of merged reads using MetaPhlAn2 and ChocoPhlAn pangenome database

Run MetaPhlAn2

`submit_metaphlan2_jobs.sh`
`metaphlan2_job.sh`

## 3b. Functional profiling of merged (and rRNA/rDNA removed) reads using HUMAnN2

Run SortMeRNA (filtering)

`sortmerna_db_index.sh` 
`submit_sortmerna_jobs.sh` 
`sortmerna_job.sh` 

Run HUMAnN2

`submit_humann2_jobs.sh`
`humann2_job.sh`
`humann2_join_job.sh`
