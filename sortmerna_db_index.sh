#!/bin/sh

echo "Start - `date`" 
#$ -N rRNA_index
#$ -q b.q
#$ -pe smp 4
#$ -cwd
#$ -j y
#$ -m abe
#$ -M <email_id>

echo ---------------------------------
echo -e `cat $PE_HOSTFILE`
echo ---------------------------------
echo SGE: qsub is running on $HOSTNAME
echo SGE: job identifier is $JOB_ID
echo SGE: job name is $JOB_NAME
echo SGE: parallel environment $PE
echo ---------------------------------

PROJ_ID="sortmerna-2.1b"
PROJ_DIR=$HOME/$PROJ_ID
FASTA_DIR=$PROJ_DIR/rRNA_databases
OUT_DIR=$PROJ_DIR/index
 
if [ ! -d ${OUT_DIR} ]; then
 mkdir -p ${OUT_DIR}
fi

TMP_NAME=`/usr/bin/mktemp -u XXXXXXXX`
TMP_DIR="/scratch/${JOB_ID}_${TMP_NAME}"
if [ -e $TMP_DIR ]; then
 echo "Error. Scratch dir already exists on `hostname`"
 exit
else
 mkdir $TMP_DIR
fi

chmod a+trwx $TMP_DIR
mkdir -p $TMP_DIR/index
echo $TMP_DIR

rsync -av $FASTA_DIR/*.fasta $TMP_DIR/index/

echo $TMP_DIR/index/*

# no spaces allowed
indexdb_rna --ref $TMP_DIR/index/rfam-5.8s-database-id98.fasta,$TMP_DIR/index/rfam-5.8s-db:\
$TMP_DIR/index/silva-arc-16s-id95.fasta,$TMP_DIR/index/silva-arc-16s-db:\
$TMP_DIR/index/silva-bac-16s-id90.fasta,$TMP_DIR/index/silva-bac-16s-db:\
$TMP_DIR/index/silva-euk-18s-id95.fasta,$TMP_DIR/index/silva-euk-18s-db:\
$TMP_DIR/index/rfam-5s-database-id98.fasta,$TMP_DIR/index/rfam-5s-db:\
$TMP_DIR/index/silva-arc-23s-id98.fasta,$TMP_DIR/index/silva-arc-23s-db:\
$TMP_DIR/index/silva-bac-23s-id98.fasta,$TMP_DIR/index/silva-bac-23s-db:\
$TMP_DIR/index/silva-euk-28s-id98.fasta,$TMP_DIR/index/silva-euk-28s-db \
 -v

/bin/rm $TMP_DIR/index/*.fasta
rsync -av $TMP_DIR/index/* $OUT_DIR
/bin/rm -rf $TMP_DIR
echo "Finish - `date`"
