#!/bin/sh

# A comprehensive metagenomic analysis from whole-genome shotgun sequencing data for microbiome studies
# https://salsa.debian.org/med-team/metaphlan2

# Download T1D data from
# https://pubs.broadinstitute.org/diabimmune/t1d-cohort/resources/metagenomic-sequence-data

echo "Start - `date`" 

echo ---------------------------------
echo -e `cat $PE_HOSTFILE`
echo ---------------------------------
echo SGE: qsub is running on $HOSTNAME
echo SGE: job identifier is $JOB_ID
echo SGE: job name is $JOB_NAME
echo SGE: parallel environment $PE
echo ---------------------------------

# sortmerna (installed in home)

TMP_NAME=`/usr/bin/mktemp -u XXXXXXXX`
TMP_DIR="/scratch/${JOB_ID}_${TMP_NAME}"
if [ -e $TMP_DIR ]; then
   echo "Error. Scratch dir already exists on `hostname`"
   exit
else
    mkdir $TMP_DIR
fi

# humann2 installed as a part of bioconda
source activate ddocent_env

# Download the ChocoPhlAn database

# humann2_databases --download chocophlan full $HOME
# HUMAnN2 configuration file updated: database_folders : nucleotide = /home/adinasarapu/chocophlan

# Download a translated search database
# While there is only one ChocoPhlAn database, users have a choice of 
# translated search databases which offer trade-offs between resolution, 
# coverage, and performance. For  EC-filtered UniRef90 database (0.8GB)

# humann2_databases --download uniref uniref90_ec_filtered_diamond $HOME
# HUMAnN2 configuration file updated: database_folders : protein = /home/adinasarapu/uniref

rsync -av $R1 $TMP_DIR
ls $TMP_DIR/*

chmod a+trwx $TMP_DIR
mkdir -p $TMP_DIR/${SID}_humann2

# Prior to running the workflow, filter your shotgun sequencing metagenome file. 
# We recommend using KneadData for metagenome quality control including removal of host reads.

# 1. Run HUMAnN2 on each of your filtered fastq files in $INPUT_DIR placing the results in $OUTPUT_DIR
# --metaphlan-options /home/adinasarapu/anaconda3/envs/ddocent_env/bin/metaphlan_databases/mpa_v20_m200.pkl
# /home/adinasarapu/anaconda3/envs/ddocent_env/bin/db_v20 cretaed manually
# --bowtie2db
# --no_map
# --bowtie2out $TMP_DIR/${SID}.bowtie2.bz2

humann2 \
 --threads $t \
 --input $TMP_DIR/${SID}_non_rRNA.fasta \
 --metaphlan-options='--input_type fasta --mpa_pkl /home/adinasarapu/anaconda3/envs/ddocent_env/bin/db_v20/mpa_v20_m200.pkl --bowtie2db /home/adinasarapu/anaconda3/envs/ddocent_env/bin/db_v20' --output-format tsv --output $TMP_DIR/${SID}_humann2

# 2. Normalize the abundance output files
# "relab" (relative abundance) or "cpm" (copies per million)
humann2_renorm_table \
 --input $TMP_DIR/${SID}_humann2/${SID}_non_rRNA_genefamilies.tsv \
 --output $TMP_DIR/${SID}_humann2/${SID}_non_rRNA_genefamilies_relab.tsv \
 --units relab

humann2_renorm_table \
 --input $TMP_DIR/${SID}_humann2/${SID}_non_rRNA_pathabundance.tsv \
 --output $TMP_DIR/${SID}_humann2/${SID}_non_rRNA_pathabundance_relab.tsv \
 --units relab

# 3. Join the output files (gene families, coverage, and abundance) from the HUMAnN2 runs from all samples into three files
 # humann2_join_tables --input $TMP_DIR/*_humann2/ --output $TMP_DIR/humann2_genefamilies.tsv --file_name genefamilies_relab
 # humann2_join_tables --input $TMP_DIR/*_humann2/ --output $TMP_DIR/humann2_pathcoverage.tsv --file_name pathcoverage
 # humann2_join_tables --input $TMP_DIR/*_humann2/ --output $TMP_DIR/humann2_pathabundance.tsv --file_name pathabundance_relab

# https://bitbucket.org/biobakery/humann2/wiki/Home#markdown-header-5-download-the-databases

# module unload bowtie2/2.2.6
source deactivate ddocent_env

/bin/rm $TMP_DIR/${SID}_non_rRNA.fasta

rsync -av $TMP_DIR/* $OUT/

/bin/rm -rf $TMP_DIR

echo "Finish - `date`"
